import pandas as pd
import numpy as np
import tensorflow as tf

# default value for AdamOptimizer is 0.001
LEARNING_RATE = 0.00005
NUM_OF_HIDDEN_CELL = 50
# Do not modify!
BATCH_SIZE = 1
MAX_TIME = 14
# Do not modify!
NUM_OF_INDICES = 1
NUM_OF_EPOCH = 40


def bogus_time_series():
    dates = pd.date_range('20130101', periods=(3 * 365))
    # s = 500.0 * np.sin(np.pi * np.arange(3 * 365) / 180.0) + 1000.0
    s = np.sin(np.pi * np.arange(3 * 365) / 180.0)

    return pd.DataFrame(s, index=dates, columns=list('s'))

df = bogus_time_series()
s = df.values


LEN_OF_TRAINING_SET = int(len(s) * 0.8)
LEN_OF_TEST_SET = len(s) - LEN_OF_TRAINING_SET


def get_a_sample(i):
    return s[i:i + MAX_TIME], s[i + MAX_TIME]


def build_graph():

    inputs = tf.placeholder(tf.float32,
                            [BATCH_SIZE, MAX_TIME, NUM_OF_INDICES])

    cell = tf.nn.rnn_cell.BasicRNNCell(NUM_OF_HIDDEN_CELL)
    # cell = tf.nn.rnn_cell.BasicLSTMCell(NUM_OF_HIDDEN_CELL)
    outputs, state = tf.nn.dynamic_rnn(cell, inputs, dtype=tf.float32)

    theta = tf.Variable(tf.truncated_normal([NUM_OF_HIDDEN_CELL, 1],
                                            stddev=0.1))
    b = tf.Variable(tf.constant(0.1, shape=[1]))

    # batch * maxtime * indices.
    sliced = tf.slice(outputs,
                      [0, MAX_TIME - 1, 0], [1, 1, NUM_OF_HIDDEN_CELL])

    # We will then take only the final output.
    # output = tf.reshape(sliced, [NUM_OF_HIDDEN_CELL])
    last = tf.reshape(sliced, [1, NUM_OF_HIDDEN_CELL])

    output = tf.nn.tanh(tf.matmul(last, theta) + b)

    sess = tf.Session()
    sess.run(tf.initialize_all_variables())

    return sess, inputs, output


# To run a sample, we need to do

# inp = get_a_sample(i)
# sess.run(output, feed_dict={inputs:[inp[0].tolist()]})

# Don't you think something' got weird? sub-array to list to list?

def train(sess, inputs, output):

    # 0-D target, target index.
    target = tf.placeholder(tf.float32, [1, 1])

    loss = tf.nn.l2_loss(tf.sub(output, target))

    # optimizer = tf.train.GradientDescentOptimizer(learning_rate=eta)
    optimizer = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE)
    minimizer = optimizer.minimize(loss)

    # AdamOptimizer needs an initialization!
    sess.run(tf.initialize_all_variables())

    for i in range(0, LEN_OF_TRAINING_SET - MAX_TIME):
        inp = get_a_sample(i)
        # print(get_a_sample(i))
        sess.run(minimizer, feed_dict={inputs: [inp[0].tolist()],
                                       target: [inp[1].tolist()]})


def test(sess, inputs, output):
    for i in range(0, LEN_OF_TEST_SET - MAX_TIME):
        inp = get_a_sample(i)
        prediction = (
            sess.run(output, feed_dict={inputs: [inp[0].tolist()]})
        )

        print i, prediction[0][0], inp[1][0]


def main():

    sess, inputs, output = build_graph()

    for i in range(0, NUM_OF_EPOCH):
        train(sess, inputs, output)

    test(sess, inputs, output)


if __name__ == "__main__":
    main()
